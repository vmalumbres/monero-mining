# monero-mining

Pequeño repositorio para poner en marchar una raspberry pi como minero de la criptomoneda monero


# Contenido

    |- mining     [Script del demonio]
    |- README.md  [Readme file]


# Funcionamiento

1. Alojar el script mining en /etc/init.d.

2. Dar permisos de ejecución `sudo chmod +x /etc/init.d/mining`

3. Actualizar enlaces `sudo update-rc.d mining defaults`
